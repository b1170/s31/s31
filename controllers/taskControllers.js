const Tasks = require("./../models/Task");

module.exports.createTask = (reqBody) => {
    return Tasks.findOne({name: reqBody.name}).then((result) => {
        if (result != null) {
            return `Task already exists`
        } else {
            let newTask = new Tasks({name: reqBody.name})
            return newTask.save().then((result) => {
                return `Saved new task`
            })
        }
    });
    return newTask.save().then(result => result);
};

module.exports.getTask = (reqBody) => {
    return Tasks.findOne({name: reqBody.name}).then((result) => result);
}

module.exports.getAllTasks = () => {
    return Tasks.find().then((result) => {return result})
}

module.exports.getTaskID = (params) => {
    return Tasks.findById(params).then((result) => {return result})
}

module.exports.updateTask = (reqBody) => {
    let updateStatus = {
        "Status": "complete"
    }
    return Tasks.findOneAndUpdate({name:reqBody.name}, updateStatus).then((result) => {return `Task Updated`})
}

module.exports.updateTaskID = (params) => {
    let updateStatus = {
        "Status": "complete"
    }
    return Tasks.findByIdAndUpdate(params, updateStatus).then((result) => {return `Task Updated`})
}

module.exports.deleteTask = (reqBody) => {
    return Tasks.findOneAndDelete({name:reqBody.name}).then((result) => {return `Task Deleted`})
}
