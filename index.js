// express module
const express = require('express');
const app = express();

// declare port - local port and available port after hosting
const PORT = process.env.PORT || 3000; // works when hosting

//require userRoutes
const userRoutes = require("./routes/userRoutes.js");
const taskRoutes = require("./routes/taskRoutes.js")

// mongoose
const mongoose = require('mongoose');

//middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//routes
//http://localhost:3000/api/users
app.use("/api/users", userRoutes);
app.use("/api/tasks", taskRoutes);

//mongodb
mongoose.connect('mongodb+srv://adminDB:FhXqMxMv8TqbV7@batch139.ndmwa.mongodb.net/s31d2', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// notification
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Failed to connect to database'));
db.once('open', () => console.log(`Successfully connected to database`));

app.listen(PORT, () => console.log(`Server running at port ${PORT}`));