const express = require("express");
const router = express.Router();
const taskController = require("./../controllers/taskControllers")

router.post("/add-task", (req, res) => {
    taskController.createTask(req.body).then(result => res.send(result))
});

router.get("/get-task", (req, res) => {
    taskController.getTask(req.body).then(result => res.send(result))
});

router.get("/get-all", (req, res) => {
    taskController.getAllTasks().then(result => res.send(result))
});

router.get("/:id", (req, res) => {
    taskController.getTaskID(req.params.id).then(result => res.send(result))
})

router.put("/update-task", (req, res) => {
    taskController.updateTask(req.body).then(result => res.send(result))
})

router.put("/:id/complete", (req, res) => {
    taskController.updateTaskID(req.params.id).then(result => res.send(result))
})

router.delete("/delete-task", (req, res) => {
    taskController.deleteTask(req.body).then(result => res.send(result))
})

module.exports = router;